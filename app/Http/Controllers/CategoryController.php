<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index(){
        $categories = Category::where('is_active',1)->get();
        if($categories){
            return response()->json([
                'status'=>200,
                'categories'=>$categories
            ]);
        }
        else {
            return response()->json([
                'status'=>200,
                'categories'=>"no Category found"
            ]);
        }
    }

    public function store(Request $request){

        try {
            $categories = Category::create($request->all());
            if($categories){
                return response()->json([
                    'status'=>200,
                    'message'=>"Category create successfully"
                ]);
            }
            else {
                return response()->json([
                    'status'=>200,
                    'categories'=>"no Category found"
                ]);
            }
        }
        catch (\Exception $e) {
            return response()->json([
                'status'=>500,
                'categories'=>$e->getMessage(),
            ]);
        }

    }

    public function delete(Request $request){
        try {
            $categories = Category::where('id',$request->id)->delete();
            if($categories){
                return response()->json([
                    'status'=>200,
                    'message'=>"Category delete successfully"
                ]);
            }
            else {
                return response()->json([
                    'status'=>200,
                    'categories'=>"no Category found against the provided id"
                ]);
            }
        }
        catch (\Exception $e) {
            return response()->json([
                'status'=>500,
                'categories'=>$e->getMessage(),
            ]);
        }

    }

    public function update(Request $request){
        try {
            $categories = Category::where('id', $request->id)->update($request->all());
            // $categories = Category::create($request->all());
            if($categories){
                return response()->json([
                    'status'=>200,
                    'message'=>"Category updated successfully"
                ]);
            }
            else {
                return response()->json([
                    'status'=>200,
                    'categories'=>"no Category found against the provided id"
                ]);
            }
        }
        catch (\Exception $e) {
            return response()->json([
                'status'=>500,
                'categories'=>$e->getMessage(),
            ]);
        }

    }
}
