<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use App\Models\Product;
use Illuminate\Http\Request;
use Stripe\StripeClient;

class PaymentController extends Controller
{
  public function payment(Request $request)
  {
    $stripe = new \Stripe\StripeClient(
      env('STRIPE_SECRET_KEY')
    );

    $payment = $stripe->paymentIntents->confirm(
      $request->pi,
      ['payment_method' => 'pm_card_visa']
    );

    return $payment;
  }

  public function generate_payment_intent(Request $request)
  {

    $product = "";
    if ($request->type == "custom") {
      $validated = $request->validate([
        'firstName' => 'required',
        'email' => 'required',
        'streetAddress' => 'required',
        'country' => 'required',
        'state' => 'required',
        'city' => 'required',
        'postalCode' => 'required',
        'phoneNumber' => 'required',
        'number' => 'required',
        'exp_month' => 'required',
        'exp_year' => 'required',
        'cvc' => 'required',
        'price' => 'required',
      ]);

      if ($request->hasFile('image')) {
        $image = $request->file('image');

        // Set the file name with extension
        $fileName = rand(111111111111, 999999999999) . $image->getClientOriginalName();

        // Move the uploaded file to a public directory
        $path = $image->storeAs('public/images', $fileName);

        // Save the image path to the database
        $imagePath = str_replace('public/', '', 'storage/' . $path);
        $request['image_path'] = $imagePath;
      }

      $product = Product::create([
        'name' => "custome_design",
        'type' => "custom",
        'price' => $request->price,
        'image_path' => $request->image_path,
      ]);
    } else {
      $validated = $request->validate([
        'firstName' => 'required',
        'email' => 'required',
        'streetAddress' => 'required',
        'country' => 'required',
        'state' => 'required',
        'city' => 'required',
        'postalCode' => 'required',
        'phoneNumber' => 'required',
        'number' => 'required',
        'exp_month' => 'required',
        'exp_year' => 'required',
        'cvc' => 'required',
        'product_id' => 'required',
      ]);
      $product = Product::where('id', $request->product_id)->first();
    }




    $stripe = new \Stripe\StripeClient(
      env('STRIPE_SECRET_KEY')
    );
    $pm =  $stripe->paymentMethods->create([
      'type' => 'card',
      'card' => [
        'number' => $request->number,
        'exp_month' => $request->exp_month,
        'exp_year' => $request->exp_year,
        'cvc' => $request->cvc,
      ],
    ]);

    $cust = $stripe->customers->create([
      'email' => $request->email,
      'name'  => $request->name,
      'phone' => isset($request->phone_number) ? $request->phone_number : '',
      'payment_method' => $pm->id,
    ]);

    $pmUpdate = $stripe->paymentMethods->attach(
      $pm->id,
      ['customer' => $cust->id]
    );

    $intent = $stripe->paymentIntents->create([
      'amount' => $product->price * 100,
      'currency' => 'usd',
      "customer" => $cust->id,
      'payment_method' => $pm->id,
      'automatic_payment_methods' => [
        'enabled' => false,
      ],
    ]);

    Payment::create([
      'amount' => $product->price,
      'firstName' => $request->firstName,
      'stripe_customer_id' => $cust->id,
      'email' => $request->email,
      'streetAddress' => $request->streetAddress,
      'country' => $request->country,
      'state' => $request->state,
      'city' => $request->city,
      'postalCode' => $request->postalCode,
      'phoneNumber' => $request->phoneNumber,
      'product_id' => ($request->type == "custom") ? $product->id : $request->product_id,
    ]);

    return $intent->id;
  }

  public function create_payment_method(Request $request)
  {
    $stripe = new \Stripe\StripeClient(
      env('STRIPE_SECRET_KEY')
    );
    $pm =  $stripe->paymentMethods->create([
      'type' => 'card',
      'card' => [
        'number' => '4242424242424242',
        'exp_month' => 8,
        'exp_year' => 2024,
        'cvc' => '314',
      ],
    ]);

    return $pm;
  }


  public function mark_paid_status(Request $request)
  {
    $data = $request->getContent();
    $format = json_decode($data);
    if ($format->type == "payment_intent.succeeded") {
      $customer =  $format->data->object->customer;
      Payment::where('stripe_customer_id', $customer)->update([
        'status' => "paid",
        'stripe_payment_id' => $format->data->object->id,
      ]);
    }
    if ($format->type == "charge.succeeded") {
      $customer =  $format->data->object->customer;
      Payment::where('stripe_customer_id', $customer)->update([
        'transaction_proof' => $format->data->object->receipt_url,
      ]);
    }
  }


  public function create_webhook(Request $request)
  {
    $stripe = new \Stripe\StripeClient(
      env('STRIPE_SECRET_KEY')
    );
    $stripe->webhookEndpoints->create([
      'url' => 'https://admin.pickaneon.com/api/mark_paid_status',
      'enabled_events' => [
        'charge.succeeded',
      ],
    ]);
  }

  public function generate_payment_token(Request $request)
  {

    $gateway = new \Braintree\Gateway([
      'environment' => 'sandbox',
      'merchantId' => env('MERCHANT_ID'),
      'publicKey' => env('PUBLIC_KEY'),
      'privateKey' => env('PRIVATE_KEY'),
    ]);

    return $clientToken = $gateway->clientToken()->generate();
  }

  public function brain_tree_payment(Request $request)
  {

    $product = "";
    if ($request->type == "custom") {
      $validated = $request->validate([
        'firstName' => 'required',
        'email' => 'required',
        'streetAddress' => 'required',
        'country' => 'required',
        'state' => 'required',
        'city' => 'required',
        'postalCode' => 'required',
        'phoneNumber' => 'required',
        'price' => 'required',
        'nonce' => 'required'
      ]);

      if ($request->hasFile('image')) {
        $image = $request->file('image');

        // Set the file name with extension
        $fileName = rand(111111111111, 999999999999) . $image->getClientOriginalName();

        // Move the uploaded file to a public directory
        $path = $image->storeAs('public/images', $fileName);

        // Save the image path to the database
        $imagePath = str_replace('public/', '', 'storage/' . $path);
        $request['image_path'] = $imagePath;
      }

      $product = Product::create([
        'name' => "custome_design",
        'type' => "custom",
        'price' => $request->price,
        'image_path' => $request->image_path,
      ]);
    } else {
      $validated = $request->validate([
        'firstName' => 'required',
        'email' => 'required',
        'streetAddress' => 'required',
        'country' => 'required',
        'state' => 'required',
        'city' => 'required',
        'postalCode' => 'required',
        'phoneNumber' => 'required',
        'product_id' => 'required',
        'nonce' => 'required'
      ]);
      $product = Product::where('id', $request->product_id)->first();
    }

    $gateway = new \Braintree\Gateway([
      'environment' => 'sandbox',
      'merchantId' => env('MERCHANT_ID'),
      'publicKey' => env('PUBLIC_KEY'),
      'privateKey' => env('PRIVATE_KEY'),
    ]);
    $result = $gateway->transaction()->sale([
      'amount' => $product->price,
      'paymentMethodNonce' => $request->nonce,
      'deviceData' => $request->device_data,
      'options' => [
        'submitForSettlement' => True
      ]
    ]);
  }
}
