<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use RealRashid\SweetAlert\Facades\Alert;

class CategoryController extends Controller
{
    public function categories_index(Request $request)
    {
        return view('admin.categories.index');
    }



    public function get_all_categories(Request $request)
    {
        $categories = Category::all();
        // return $customers;
        if (request()->ajax()) {
            return DataTables::of($categories)
                ->addIndexColumn()
                ->editColumn('action', function ($categorie) {

                    return '<a type="button" onclick="delete_category(' . $categorie->id . ')" class="btn btn-danger"><i class="fa-solid fa-trash-arrow-up"></i></a>';
                })
                ->editColumn('parent', function ($categorie) {

                    return ($categorie->parent_id) ? Category::where('id', $categorie->parent_id)->value('name') : 'N/A';
                })
                ->editColumn('image', function ($categorie) {

                    return view('admin.categories.image', compact('categorie'));
                })

                ->rawColumns(['action', 'image'])
                ->toJson();
        }
        return view('admin.categories.index');
    }

    public function add_category_index()
    {

        $categories = Category::all();

        return view('admin.categories.add_category', compact('categories'));
    }

    public function add_category(Request $request)
    {
        $validated = $request->validate([
            'slug' => 'required|unique:categories,slug',
            'name' => 'required',
        ]);
        if ($request->hasFile('image')) {
            $image = $request->file('image');

            // Set the file name with extension
            $fileName = rand(111111111111, 999999999999) . $image->getClientOriginalName();

            // Move the uploaded file to a public directory
            $path = $image->storeAs('public/images', $fileName);

            // Save the image path to the database
            $imagePath = str_replace('public/', '', 'storage/' . $path);
            $request['image_path'] = $imagePath;
        }

        $categories = Category::create($request->except('_token', 'image'));

        Alert::success('Success', 'Product Added Successfully');
        return view('admin.categories.index');
    }

    public function delete_category($id)
    {
        $delete = Category::where('id', $id)->delete();
        if ($delete) {
            return "success";
        }
    }
}
