<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Console\View\Components\Alert as ComponentsAlert;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use RealRashid\SweetAlert\Facades\Alert;
use Stripe\Service\ProductService;

class ProductController extends Controller
{
    public function get_all_products(Request $request)
    {
        $products = Product::where('type', null)->with('category')->get();
        // return $customers;
        if (request()->ajax()) {
            return DataTables::of($products)
                ->addIndexColumn()
                ->editColumn('action', function ($product) {

                    return '<a type="button" onclick="delete_product(' . $product->id . ')" class="btn btn-danger"><i class="fa-solid fa-trash-arrow-up"></i></a> <a type="button" onclick="edit_product(' . $product->id . ')" class="btn btn-dark"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
                })
                ->editColumn('category', function ($product) {

                    return $product->category->name;
                })
                ->editColumn('status', function ($product) {

                    return ($product->is_active) ? '<a onclick="change_status(' . $product->id . ')" style="cursor:pointer;"><span class="badge badge-success">active</span></a>' : '<a style="cursor:pointer;" onclick="change_status(' . $product->id . ')"><span class="badge badge-danger">disabled</span></a>';
                })
                ->editColumn('image', function ($product) {

                    return view('admin.product.image', compact('product'));
                })
                ->rawColumns(['action', 'status', 'image'])
                ->toJson();
        }
        return view('admin.product.index');
    }

    public function add_products_index(Request $request)
    {
        $categories = Category::all();
        return view('admin.product.add_product', compact('categories'));
    }
    public function add_product(Request $request)
    {
        // return $request->all();
        $validated = $request->validate([
            'category_id' => 'required',
        ]);

        $stripe = new \Stripe\StripeClient(
            'sk_test_51MhQboKAMa23hA3IuABOXfUqIiHuh2TBjADpJ5e3c9CC52elN8u1L7PW9VPjCNyooRFCRhLOeM4SZe3aMjotUpJH009SiSjTVw'
        );

        $product = $stripe->products->create([
            'name' => $request->name,
        ]);

        $stripe->prices->create([
            'unit_amount' => $request->price,
            'currency' => 'usd',
            'product' => $product->id,
        ]);

        if ($request->hasFile('image')) {
            $image = $request->file('image');

            // Set the file name with extension
            $fileName = rand(111111111111, 999999999999) . $image->getClientOriginalName();

            // Move the uploaded file to a public directory
            $path = $image->storeAs('public/images', $fileName);

            // Save the image path to the database
            $imagePath = str_replace('public/', '', 'storage/' . $path);
            $request['image_path'] = $imagePath;
        }

        $request['stripe_product_id'] = $product->id;
        $products = Product::create($request->except('_token', 'image'));



        Alert::success('Success', 'Product Added Successfully');

        return view('admin.product.index');
    }

    public function delete_product($id)
    {
        $delete = Product::where('id', $id)->delete();
        if ($delete) {
            return "success";
        }
    }

    public function change_status($id)
    {
        $product = Product::where('id', $id)->value('is_active');
        if ($product) {
            $update = Product::where('id', $id)->update(['is_active' => 0]);
        } else {
            $update = Product::where('id', $id)->update(['is_active' => 1]);
        }

        if ($update) {
            return "success";
        }
    }


    public function edit_product(Request $request)
    {
        $product = Product::where('id', $request->id)->first();
        if (isset($request->name)) {
            $product->name = $request->name;
        }
        if (isset($request->description)) {
            $product->description = $request->description;
        }
        if (isset($request->price)) {
            $product->price = $request->price;
        }
        if (isset($request->features)) {
            $product->features = $request->features;
        }
        if (isset($request->color)) {
            $product->color = $request->color;
        }
        if (isset($request->size)) {
            $product->size = $request->size;
        }
        if ($request->hasFile('image')) {
            $image = $request->file('image');

            // Set the file name with extension
            $fileName = rand(111111111111, 999999999999) . $image->getClientOriginalName();

            // Move the uploaded file to a public directory
            $path = $image->storeAs('public/images', $fileName);

            // Save the image path to the database
            $imagePath = str_replace('public/', '', 'storage/' . $path);
            $request['image_path'] = $imagePath;


            $product->image_path = $request->image_path;
        }

        $product->save();
        Alert::success('Success', 'Product Edit Successfully');
        return view('admin.product.index');
    }
}
