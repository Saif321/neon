<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Payment;
use App\Models\Product;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class PaymentController extends Controller
{
    public function payments_index(Request $requests)
    {
        return view('admin.payments.index');
    }

    public function get_all_payments(Request $request)
    {
        $payments = Payment::all();
        // return $customers;
        if (request()->ajax()) {
            return DataTables::of($payments)
                ->addIndexColumn()
                // ->editColumn('action', function ($payments) {

                //     return '<a type="button" onclick="delete_payments(' . $payments->id . ')" class="btn btn-danger"><i class="fa-solid fa-trash-arrow-up"></i></a>';
                // })
                // ->editColumn('category', function ($payments) {

                //     return $payments->category->name;
                // })
                ->editColumn('status', function ($payment) {

                    return ($payment->status == "paid") ? '<a  style="cursor:pointer;"><span class="badge badge-primary">paid</span></a>' : '<a style="cursor:pointer;"><span class="badge badge-danger">Not paid</span></a>';
                })
                ->editColumn('product', function ($payment) {

                    return '<a onclick="view_product(' . $payment->product_id . ')" style="cursor:pointer;"><span class="badge badge-warning">view details</span></a>';
                    // return ($payment->product_id) ? Product::where('id', $payment->product_id)->value('name') . '||' . $payment->product_id : "";
                })
                ->editColumn('transaction', function ($payment) {

                    return ($payment->transaction_proof) ? '<a href="' . $payment->transaction_proof . '" style="cursor:pointer;"><span class="badge badge-dark">view reciept</span></a>' : "";
                })
                ->editColumn('delievery', function ($payment) {

                    return ($payment->delievery_status == "Not delieverd") ? '<a onclick="mark_deliever(' . $payment->id . ')" style="cursor:pointer;"><span class="badge badge-danger">mark delieverd</span></a>' : '<a  style="cursor:pointer;"><span class="badge badge-success">' . $payment->delievery_status . '</span></a>';
                })


                // ->editColumn('image', function ($product) {

                //     return view('admin.product.image', compact('product'));
                // })


                ->rawColumns(['product', 'status', 'image', 'transaction', 'delievery'])
                ->toJson();
        }
        return view('admin.payments.index');
    }


    public function admin_product_get($id)
    {

        return Product::where('id', $id)->first();
    }


    public function order_status(Request $request, $id)
    {
        $order = Payment::where('id', $id)->update(['delievery_status' => "delivered"]);
    }
}
