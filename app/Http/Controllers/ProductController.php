<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(Request $request)
    {

        $products = Product::where('is_active', 1)->where('type', null)->get();

        if ($products) {
            return response()->json([
                'status' => 200,
                'products' => $products
            ]);
        } else {
            return response()->json([
                'status' => 200,
                'products' => "no product found"
            ]);
        }
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'category_id' => 'required',
        ]);
        try {

            if ($request->hasFile('media')) {
                $image = $request->file('media');

                // Set the file name with extension
                $fileName = rand(111111111111, 999999999999) . $image->getClientOriginalName();

                // Move the uploaded file to a public directory
                $path = $image->storeAs('public/images', $fileName);

                // Save the image path to the database
                $imagePath = str_replace('public/', '', 'storage/' . $path);
                $request['image_path'] = $imagePath;
            }


            $products = Product::create($request->except('media'));
            if ($products) {
                return response()->json([
                    'status' => 200,
                    'message' => "product create successfully"
                ]);
            } else {
                return response()->json([
                    'status' => 200,
                    'products' => "no product found"
                ]);
            }
        } catch (\Exception $e) {
            return response()->json([
                'status' => 500,
                'products' => $e->getMessage(),
            ]);
        }
    }

    public function delete(Request $request)
    {
        $validated = $request->validate([
            'product_id' => 'required',
        ]);
        try {
            $products = Product::where('id', $request->product_id)->delete();
            if ($products) {
                return response()->json([
                    'status' => 200,
                    'message' => "product delete successfully"
                ]);
            } else {
                return response()->json([
                    'status' => 200,
                    'products' => "no product found against the provided id"
                ]);
            }
        } catch (\Exception $e) {
            return response()->json([
                'status' => 500,
                'products' => $e->getMessage(),
            ]);
        }
    }

    public function update(Request $request)
    {
        $validated = $request->validate([
            'product_id' => 'required',
        ]);
        if ($request->hasFile('media')) {
            $image = $request->file('media');

            // Set the file name with extension
            $fileName = rand(111111111111, 999999999999) . $image->getClientOriginalName();

            // Move the uploaded file to a public directory
            $path = $image->storeAs('public/images', $fileName);

            // Save the image path to the database
            $imagePath = str_replace('public/', '', 'storage/' . $path);
            $request['image_path'] = $imagePath;
        }
        try {
            $products = Product::where('id', $request->product_id)->update($request->except('product_id', 'media'));
            // $products = Product::create($request->all());
            if ($products) {
                return response()->json([
                    'status' => 200,
                    'message' => "product updated successfully"
                ]);
            } else {
                return response()->json([
                    'status' => 200,
                    'products' => "no product found against the provided id"
                ]);
            }
        } catch (\Exception $e) {
            return response()->json([
                'status' => 500,
                'products' => $e->getMessage(),
            ]);
        }
    }

    public function get_specific_product(Request $request)
    {
        $products = Product::where('id', $request->product_id)->first();

        if ($products) {
            return response()->json([
                'status' => 200,
                'products' => $products
            ]);
        } else {
            return response()->json([
                'status' => 200,
                'products' => "no product found"
            ]);
        }
    }

    public function get_products_against_category(Request $request)
    {
        $category = Category::where('id', $request->category_id)->with('products')->get();
        if (count($category) > 0) {
            return response()->json([
                'status' => '200',
                'products' => $category,
            ]);
        } else {
            return response()->json([
                'status' => '200',
                'products' => "no product found against category",
            ]);
        }
    }
}
