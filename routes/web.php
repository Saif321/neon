<?php

use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\PaymentController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\ProfileController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return Inertia::render('Welcome', [
//         'canLogin' => Route::has('login'),
//         'canRegister' => Route::has('register'),
//         'laravelVersion' => Application::VERSION,
//         'phpVersion' => PHP_VERSION,
//     ]);
// });
Route::get('/', function () {
    return redirect('login');
});

// Route::get('/dashboard', function () {
//     return view('admin.layouts.dashboard');
// })->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    Route::get('/products_index', [UserController::class, 'index'])->name('products_index');
    //
    Route::get('/get_all_products', [ProductController::class, 'get_all_products'])->name('get_all_products');
    Route::get('/add_products_index', [ProductController::class, 'add_products_index'])->name('add_products_index');

    Route::post('/add_product', [ProductController::class, 'add_product'])->name('add_product');
    Route::get('/admin/product/delete/{id}', [ProductController::class, 'delete_product']);

    Route::get('/categories_index', [CategoryController::class, 'categories_index'])->name('categories_index');

    Route::get('/get_all_categories', [CategoryController::class, 'get_all_categories'])->name('get_all_categories');

    Route::get('/add_category_index', [CategoryController::class, 'add_category_index'])->name('add_category_index');

    Route::post('/add_category', [CategoryController::class, 'add_category'])->name('add_category');

    Route::get('/admin/category/delete/{id}', [CategoryController::class, 'delete_category']);


    Route::get('admin/product/status/{id}', [ProductController::class, 'change_status']);

    Route::get('/dashboard', [DashboardController::class, 'dashboard'])->name('dashboard');

    Route::get('payments_index', [PaymentController::class, 'payments_index'])->name('payments_index');

    Route::get('get_all_payments', [PaymentController::class, 'get_all_payments'])->name('get_all_payments');

    Route::get('admin/product/get/{id}', [PaymentController::class, 'admin_product_get']);

    Route::get('admin/product/delievered/{id}', [PaymentController::class, 'order_status']);

    Route::get('signout', [UserController::class, 'signout'])->name('signout');

    Route::post('edit_product', [ProductController::class, 'edit_product'])->name('edit_product');
});


require __DIR__ . '/auth.php';
