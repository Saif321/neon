<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\ProductController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


// products

// Route::post('create_product', [ProductController::class, 'store']);
// Route::post('delete_product', [ProductController::class, 'delete']);
// Route::post('update_product', [ProductController::class, 'update']);
Route::post('get_products', [ProductController::class, 'index']);

// Route::post('get_product', [ProductController::class, 'get_product']);

Route::post('get_specific_product', [ProductController::class, 'get_specific_product']);


Route::post('get_products_against_category', [ProductController::class, 'get_products_against_category']);

// categories


// Route::post('create_category', [CategoryController::class, 'store']);
// Route::post('delete_category', [CategoryController::class, 'delete']);
// Route::post('update_category', [CategoryController::class, 'update']);
Route::post('get_categories', [CategoryController::class, 'index']);



// payments

Route::post('payment', [PaymentController::class, 'payment']);

Route::post('generate_payment_intent', [PaymentController::class, 'generate_payment_intent']);


Route::post('td_secure_payment', [PaymentController::class, 'td_secure_payment']);


Route::post('create_payment_method', [PaymentController::class, 'create_payment_method']);


Route::post('mark_paid_status', [PaymentController::class, 'mark_paid_status']);

Route::post('create_webhook', [PaymentController::class, 'create_webhook']);


// braintree

Route::get('generate_payment_token', [PaymentController::class, 'generate_payment_token']);

Route::get('brain_tree_payment', [PaymentController::class, 'brain_tree_payment']);
